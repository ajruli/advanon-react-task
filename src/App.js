import React from 'react';
import UserList from './UserList'
import './App.css';

class App extends React.Component {
    render() {
        return (
            <div className="App">
                <div className="App-header">
                    <h4>List Of GitHub Users</h4>
                </div>
                <div className="App-body">
                    <UserList/>
                </div>
            </div>
        )
    }
}

export default App;
