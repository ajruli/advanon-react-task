import React, { PropTypes } from 'react';
import './Bio.css';

const Bio = ({ avatar, login }) => {
    return (
        <div>
            <div className="imgWrapper">
                <img src={avatar} alt={login} />
            </div>
            <h1>{login}</h1>
        </div>
    )
};

Bio.propTypes = {
    avatar: PropTypes.string,
    login: PropTypes.string
}

export default Bio;