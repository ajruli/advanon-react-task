import React from 'react'
import Loading from 'react-loading'
import { ListGroup, ListGroupItem } from 'react-bootstrap';
import Bio from './Bio'

class UserList extends React.Component {
    constructor() {
        super();
        this.state = {
            loading: true,
            data: null
        }
    }

    componentWillMount() {
        this.getUser();
    }

    getUser() {
        // I have wrapped it with timeout because the response time if pretty fast
        // and loading is not visible.
        setTimeout(()=>{
            fetch('https://api.github.com/users')
                .then(data => data.json())
                .then(data => {
                    this.setState({
                        data: data,
                        loading: false
                    })
                })

        }, 2*1000)

    }

    render() {
        if (this.state.loading) {
            return(
                <Loading type='bubbles' color='#000' />
            )
        }
        if(this.state.data) {
            const data = this.state.data;
            var userList = data.map((user, index) => {
                return(
                    <ListGroupItem key={index} >
                        <Bio
                            avatar={user.avatar_url}
                            login={user.login}
                        />
                    </ListGroupItem>
                )
            });
        }
        return (
            <ListGroup>
                {userList}
            </ListGroup>
        )
    }
}

export default UserList;